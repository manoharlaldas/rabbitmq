#!/usr/bin/eve ruby
require 'bunny'
#connect to RabbitMQ server
connection = Bunny.new(automatically_recover: false)
connection.start

#create a channel, where most of the API resides
channel = connection.create_channel

#to send declear a queue then we can publish a message to the queue.
queue = channel.queue('hello')

channel.default_exchange.publish('Hello World RabbitMQ!', routing_key: queue.name)
puts " [x] Send 'Hello World!'"
connection.close