class Publisher
  # In order to publish message we need a exchange name.
  #Note that RabbitMQ does not care about the payload
  # We will be using JSON-encoded strings
  def self.publish(exchange, message = {})
    #grab the fanout exchange
    x = channel.fanout("blog.#{exchange}")
    #simple publish message
    x.publish(message.to_json)
  end
  
  def self.channel
    #Note :- a ||= b =>( a || a =b) means if left hand side of || is true no need to check right hand side
    @channel ||= connection.create_channel
  end

  #We are using default settings here
  def self.connection
    @connection ||= Bunny.new.tap do |c|
      c.start
    end
  end
end