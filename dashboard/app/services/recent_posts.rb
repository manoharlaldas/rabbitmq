class RecentPosts
	KEY = "recent_posts" # redis key
	STORE_LIMIT = 5      # how many posts should be kept

	# Get list of recent posts from redis
	# Since redis stores data in binary text format
	# we need to parse each list item as JSON
	def self.list(limit = STORE_LIMIT)
		$redis.lrange(KEY, 0, limit-1).map do |raw_post|
			JSON.parse(raw_post).with_indifferent_access
		end
	end

	# Push new post to list and trim it's size
	# to limit required storage space
	# 'raw_post' is really a JSON string
	# so there is no need to encode it as JSON

	def self.push(raw_post)
		$redis.lpush(KEY, raw_post)
		$redis.ltrim(KEY, 0, STORE_LIMIT-1)
		# we automatically push new items to the front of the list using LPUSH
		# we always fetch the first few items using LRANGE
		# we can easily limit the storage using LTRIM.
		# There is no need to any sorting or filtering
	end
end