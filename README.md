README
     This is a simple app based on Ruby on Rails.

APPLICATION
      This app will allow users to see there all expenses and to whom they have given it. It will help user to list there expenses by the name and comment.

REQUIREMENT
Ruby Version: ruby 2.2.1p85
Rails Version: Rails 4.2.1 Database: mysql Ver 14.14

GEM
gem 'bunny'
gem 'redis-rails'
gem 'redis-namespace'
gem 'sneakers'

INSTALL AND START RABBITMQ 
brew install rabbitmq (for mac user)
$ /usr/local/opt/rabbitmq/sbin/rabbitmq-server"

TO RUN RABBITMQ RAKEFILE
rake rabbitmq:setup

TO RUN WORKER IN Dashboard APPLICATION
Run command "WORKERS=PostsWorker rake sneakers:run"

REDIS SETUP
# dashboard/config/initializers/redis.rb
$redis = Redis::Namespace.new("dashboard:#{Rails.env}", redis: Redis.new)

SNEAKERS SETUP
# dashboard/Rakefile

# load sneakers tasks
require 'sneakers/tasks'

Rails.application.load_tasks

# dashboard/config/initializers/sneakers.rb
Sneakers.configure({})
Sneakers.logger.level = Logger::INFO # the default DEBUG is too noisy

BUNNY
It is a Ruby client for RabbitMQ

SNEAKERS
High Performance Background Jobs on Ruby

STATUS
still under development.